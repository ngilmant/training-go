package client

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/ngilmant/training-go/pkg/backend"
	"os"
)

type HttpConfig struct {
	BaseUrl   string
	AddUrl    string
	ListUrl   string
	FindUrl   string
	UpdateUrl string
	DeleteUrl string
}

var rootCmd = &cobra.Command{
	Use:   "client",
	Short: "a client CLI to interact with http server cache",
	Long:  `a client CLI to interact with the http server cache.`,
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

//http config reference
var httpConfig HttpConfig

//Key, Value args parameter reference
var key, value string

func init() {
	//TODO Config could be fetched from a service delegate
	httpConfig = HttpConfig{
		"http://localhost:8080",
		"/addItem",
		"/listItems",
		"/findItem/",
		"/updateItem",
		"/deleteItem/",
	}
}

func addFlags(c *cobra.Command, valueOptional bool) {
	c.Flags().StringP("key", "k", "", "Key of the cache map item")

	if !valueOptional {
		c.Flags().StringP("value", "v", "", "Value of the cache map item")
	}
}

func createItemRequest(cmd *cobra.Command, keyOptional bool, valueOptional bool) *backend.ItemRequest {

	if !keyOptional {
		k_, err := cmd.Flags().GetString("key")
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		if len(k_) == 0 {
			fmt.Println("-key -k Flags is mandatory")
			os.Exit(1)
		}
		key = fmt.Sprintf(k_)
	}

	if !valueOptional {
		v_, err := cmd.Flags().GetString("value")
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		if len(v_) == 0 {
			fmt.Println("-value -v Flags is mandatory")
			os.Exit(1)
		}
		value = fmt.Sprintf(v_)
	}

	defer cleanFlags()

	//Parse
	return &backend.ItemRequest{
		key,
		backend.Item{
			value,
		},
	}
}

func cleanFlags() {
	key = ""
	value = ""
}
