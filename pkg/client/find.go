package client

import (
	"encoding/json"
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/ngilmant/training-go/pkg/backend"
	"io/ioutil"
	"net/http"
	"os"
)

// listCmd represents the list command
var findCmd = &cobra.Command{
	Use:   "find",
	Short: "Find an item in the cache map",
	Long:  `Find an item in the cache map`,
	Run:   find,
}

func init() {
	rootCmd.AddCommand(findCmd)
	addFlags(findCmd, true)
}

func find(cmd *cobra.Command, args []string) {
	req := createItemRequest(cmd, false, true)

	resp, err := http.Get(httpConfig.BaseUrl + httpConfig.FindUrl + req.Key)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %v\n", err)
		os.Exit(1)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		fmt.Printf("The HTTP body read failed with error %v\n", err)
		os.Exit(1)
	}

	if resp.StatusCode != http.StatusOK {
		fmt.Printf("The HTTP request failed with HTTP Code : %v - %v", resp.StatusCode, string(body))
		os.Exit(1)
	}

	item := &backend.Item{}
	json.Unmarshal(body, item)

	fmt.Printf("Item Found :\nValue: %v\n", item.Value)
}
