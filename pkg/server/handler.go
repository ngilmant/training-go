package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/ngilmant/training-go/pkg/backend"
	"io/ioutil"
	"net/http"
)

type Handler struct {
	CM *backend.Manager
}

func NewHandler(manager *backend.Manager) *Handler {
	return &Handler{
		CM: manager,
	}
}

/*`
Setup Routes
*/
func (h *Handler) SetupRoute(mux *mux.Router) {
	mux.HandleFunc("/addItem", h.addItem).Methods(http.MethodPost)
	mux.HandleFunc("/listItems", h.listItem).Methods(http.MethodGet)
	mux.HandleFunc("/findItem/{key}", h.findItem).Methods(http.MethodGet)
	mux.HandleFunc("/updateItem", h.updateItem).Methods(http.MethodPut)
	mux.HandleFunc("/deleteItem/{key}", h.deleteItem).Methods(http.MethodDelete)
	mux.Use(jsonHeaderMiddleware)
}

/*
	Add Item Handler
*/
func (h *Handler) addItem(w http.ResponseWriter, r *http.Request) {
	req, err := parseRequest(r)
	if err != nil {
		http.Error(w, fmt.Sprint(err), http.StatusBadRequest)
		return
	}
	createdItem, err := h.CM.Add(req.Key, req.Element.Value)
	if err != nil {
		http.Error(w, fmt.Sprintf("Error crating an Item %v\n", err), http.StatusBadRequest)
		return
	}
	json.NewEncoder(w).Encode(createdItem)
}

/*
	List Item Handler
*/
func (h *Handler) listItem(w http.ResponseWriter, r *http.Request) {
	items := h.CM.List()
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(items)
}

/*
	Find Item Handler
*/
func (h *Handler) findItem(w http.ResponseWriter, r *http.Request) {
	key, ok := mux.Vars(r)["key"]

	if !ok {
		http.Error(w, fmt.Sprintf("No Path parameter found in your request for key: %v\n", key), http.StatusBadRequest)
		return
	}
	item, err := h.CM.Find(key)
	if err != nil {
		http.Error(w, fmt.Sprintf("key %v Not Found", key), http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(item)
}

/*
	Update Item Handler
*/
func (h *Handler) updateItem(w http.ResponseWriter, r *http.Request) {
	req, err := parseRequest(r)
	if err != nil {
		http.Error(w, fmt.Sprint(err), http.StatusBadRequest)
		return
	}
	//Asynchronous update
	h.CM.UpdateChannel() <- *req
}

/*
	Delte Item Handler
*/
func (h *Handler) deleteItem(w http.ResponseWriter, r *http.Request) {
	key, ok := mux.Vars(r)["key"]
	if !ok {
		http.Error(w, fmt.Sprintf("No Path parameter found in your request for key: %v\n", key), http.StatusBadRequest)
		return
	}
	//Asynchronous delete
	h.CM.DeleteChannel() <- key
}

/*
	Utilities
*/
func parseRequest(r *http.Request) (req *backend.ItemRequest, err error) {
	//var req backend.ItemRequest
	b, _ := ioutil.ReadAll(r.Body)
	err = json.Unmarshal(b, &req)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("Error Unmarshalling Request : %v\n", err))
	}
	return req, nil
}
