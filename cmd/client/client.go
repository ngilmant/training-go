package main

import (
	"gitlab.com/ngilmant/training-go/pkg/client"
)

func main() {
	client.Execute()
}
