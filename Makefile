#Enable GO Mod
GO111MODULE=on

VERSION := 0.1.0-SNAPSHOT

gs:
	rm -rf $(GOPATH)/bin/server
	go build -v -i -o $(GOPATH)/bin/server ./cmd/server

gc:
	rm -rf $(GOPATH)/bin/client
	go build -v -i -o $(GOPATH)/bin/client ./cmd/client
