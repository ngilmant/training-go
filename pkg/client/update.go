package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/spf13/cobra"
	"net/http"
	"os"
	"time"
)

// updateCmd represents the update command
var updateCmd = &cobra.Command{
	Use:   "update",
	Short: "Update an Item to the cache map",
	Long:  `Update an Item to the cache map`,
	Run:   update,
}

func init() {
	rootCmd.AddCommand(updateCmd)
	addFlags(updateCmd, false)
}

func update(cmd *cobra.Command, args []string) {
	req := createItemRequest(cmd, false, false)

	body, err := json.Marshal(req)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	//Create Update(PUT) Request
	client := &http.Client{
		Timeout: 5 * time.Second,
	}

	request, err := http.NewRequest(http.MethodPut, httpConfig.BaseUrl+httpConfig.UpdateUrl, bytes.NewReader(body))
	//Call the Server
	resp, err := client.Do(request)

	if err != nil {
		fmt.Printf("The HTTP request failed with error %v\n", err)
		os.Exit(1)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		fmt.Printf("The HTTP request failed with HTTP Code : %v - %v", resp.StatusCode, string(body))
		os.Exit(1)
	}

	fmt.Printf("Cache updated - call list command to see updated value\n")
}
