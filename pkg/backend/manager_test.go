package backend

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
	"time"
)

const (
	key_1        = "key_1"
	key_2        = "key_2"
	value_1      = "value_1"
	value_2      = "value_2"
	value_update = "updated Value"
)

func TestAddElement(t *testing.T) {
	testName := "Test Add Item"
	fmt.Println(testName)
	cm := CreateManager(context.Background())
	cm.Add(key_1, value_1)
	require.Len(t, cm.cache, 1)
}

func TestAddElementThroughStoreInterface(t *testing.T) {
	testName := "Test Add Item Through Interface"
	fmt.Println(testName)
	var store Store
	store = NewManager()
	store.Add(key_2, value_2)
	require.Len(t, store.List(), 1)
}

func TestDeleteElement(t *testing.T) {
	testName := "Test Delete Item"
	fmt.Println(testName)
	cm := CreateManager(context.Background())

	//Add Elements
	cm.Add(key_1, value_1)
	require.Len(t, cm.cache, 1)
	cm.Add(key_2, value_2)
	require.Len(t, cm.cache, 2)

	//Delete Key_1
	cm.deleteChannel <- key_1

	time.Sleep(1 * time.Second)
	require.Len(t, cm.cache, 1)
	require.Equal(t, cm.cache[key_2].Value, value_2)
}

func TestUpdateElement(t *testing.T) {
	testName := "Test Update Item"
	fmt.Println(testName)
	cm := CreateManager(context.Background())

	//Add Elements
	cm.Add(key_1, value_1)
	require.Len(t, cm.cache, 1)
	cm.Add(key_2, value_2)
	require.Len(t, cm.cache, 2)

	//Update Item
	updateRequest := ItemRequest{key_2, Item{value_update}}
	cm.updateChannel <- updateRequest

	time.Sleep(1 * time.Second)
	require.Equal(t, cm.cache[key_2].Value, value_update)
}

func TestListElement(t *testing.T) {
	testName := "Test List Item"
	fmt.Println(testName)

	cm := CreateManager(context.Background())

	//Add Elements
	cm.Add(key_1, value_1)
	require.Len(t, cm.cache, 1)
	cm.Add(key_2, value_2)
	require.Len(t, cm.cache, 2)

	elements := cm.List()
	require.Len(t, elements, 2)
}

func TestFindElement(t *testing.T) {
	testName := "Test Find Item"
	fmt.Println(testName)

	cm := CreateManager(context.Background())
	//Add Elements
	cm.Add(key_1, value_1)
	require.Len(t, cm.cache, 1)
	cm.Add(key_2, value_2)
	require.Len(t, cm.cache, 2)

	element, err := cm.Find(key_2)
	require.Empty(t, err)
	require.Equal(t, element.Value, value_2)
}

func TestChannelsClosure(t *testing.T) {
	testName := "Test Channels Closure"
	fmt.Println(testName)

	ctx, cancel := context.WithCancel(context.Background())
	cm := CreateManager(ctx)

	assert.NotNil(t, cm.updateChannel)
	cancel()

	time.Sleep(1 * time.Second)
	_, ok := <-cm.updateChannel

	assert.False(t, ok)
}

func CreateManager(ctx context.Context) *Manager {
	cm := NewManager()
	//Start Listener's.
	cm.Start(ctx)
	return cm
}
