package backend

import (
	"context"
	"fmt"
	"sync"
)

type Item struct {
	Value string `json:"value"`
}

type ItemRequest struct {
	Key     string `json:"key"`
	Element Item   `json:"item"`
}

type Store interface {
	List() map[string]Item
	Find(key string) (*Item, error)
	Add(key string, value string) (*Item, error)
	UpdateChannel() chan ItemRequest
	DeleteChannel() chan string
	// Start starts channel listener don't forget to close update and delete channel when ctx.Done() ,
	// Start must be call on go routine and be close when ctx.Done()
	// USE A select in a inifinite loop
	Start(ctx context.Context)
}

type Manager struct {
	sync.RWMutex  // Mutex.
	cache         map[string]Item
	updateChannel chan ItemRequest
	deleteChannel chan string
}

func (m *Manager) DeleteChannel() chan string {
	return m.deleteChannel
}

func (m *Manager) UpdateChannel() chan ItemRequest {
	return m.updateChannel
}

func NewManager() *Manager {
	return &Manager{
		cache:         make(map[string]Item),
		updateChannel: make(chan ItemRequest),
		deleteChannel: make(chan string),
	}
}

func (m *Manager) List() map[string]Item {
	m.RLock()
	defer m.RUnlock()
	return m.cache
}

func (m *Manager) Find(key string) (*Item, error) {
	m.RLock()
	defer m.RUnlock()
	e, ok := m.cache[key]
	if !ok {
		return nil, fmt.Errorf("No Item found in the cache for the Key [%v]", key)
	}
	return &e, nil
}

func (m *Manager) Add(key string, value string) (*Item, error) {
	if len(key) == 0 || len(value) == 0 {
		return nil, fmt.Errorf("Invalid arguments detected Key:'%v' - Value: '%v'", key, value)
	}
	m.Lock()
	defer m.Unlock()
	element := Item{value}
	_, ok := m.cache[key]
	if ok {
		return nil, fmt.Errorf("The key %v already exist. update it if you want to change its value!", key)
	}
	m.cache[key] = element
	return &element, nil
}

func (m *Manager) update(r ItemRequest) {
	m.Lock()
	defer m.Unlock()
	m.cache[r.Key] = r.Element
}

func (m *Manager) delete(key string) {
	m.Lock()
	defer m.Unlock()
	delete(m.cache, key)
}

func (m *Manager) listenUpdate() {
	for u := range m.updateChannel {
		m.update(u)
	}
}

func (m *Manager) listenDelete() {
	for d := range m.deleteChannel {
		m.delete(d)
	}
}

func (m *Manager) Start(ctx context.Context) {
	//Start Listener.
	go m.listenUpdate()
	go m.listenDelete()
	//Listen Context.
	go func() {
		for {
			select {
			case <-ctx.Done():
				fmt.Printf("Closing Channels \n")
				close(m.updateChannel)
				close(m.deleteChannel)
				return
			}
		}
	}()
	//Started...
}
