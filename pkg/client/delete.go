package client

import (
	"fmt"
	"github.com/spf13/cobra"
	"net/http"
	"os"
	"time"
)

// deleteCmd represents the delete command
var deleteCmd = &cobra.Command{
	Use:   "delete",
	Short: "Delete an Item to the cache map",
	Long:  `Delete an Item to the cache map`,
	Run:   delete,
}

func init() {
	rootCmd.AddCommand(deleteCmd)
	addFlags(deleteCmd, true)
}

func delete(cmd *cobra.Command, args []string) {
	req := createItemRequest(cmd, false, true)

	//Create Delete(DELETE) Request
	client := &http.Client{
		Timeout: 5 * time.Second,
	}

	request, err := http.NewRequest(http.MethodDelete, httpConfig.BaseUrl+httpConfig.DeleteUrl+req.Key, nil)
	//Call the Server
	resp, err := client.Do(request)

	if err != nil {
		fmt.Printf("The HTTP request failed with error %v\n", err)
		os.Exit(1)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		fmt.Printf("The HTTP request failed with HTTP Code : %v - %v", resp.StatusCode, req.Key)
		os.Exit(1)
	}

	fmt.Printf("Cache Item deleted - call list command to see updated value\n")
}
