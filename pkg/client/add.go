package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/ngilmant/training-go/pkg/backend"
	"io/ioutil"
	"net/http"
	"os"
)

// addCmd represents the add command
var addCmd = &cobra.Command{
	Use:   "add",
	Short: "Add an Item to the cache map",
	Long:  `Add an Item to the cache map`,
	Run:   add,
}

func init() {
	rootCmd.AddCommand(addCmd)
	addFlags(addCmd, false)
}

func add(cmd *cobra.Command, args []string) {
	req := createItemRequest(cmd, false, false)

	body, err := json.Marshal(req)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	//Call the Server
	resp, err := http.Post(httpConfig.BaseUrl+httpConfig.AddUrl, "application/json", bytes.NewReader(body))

	if err != nil {
		fmt.Printf("The HTTP request failed with error %v\n", err)
		os.Exit(1)
	}

	defer resp.Body.Close()
	body, err = ioutil.ReadAll(resp.Body)

	if err != nil {
		fmt.Printf("The HTTP body read failed with error %v\n", err)
		os.Exit(1)
	}

	if resp.StatusCode != http.StatusOK {
		fmt.Printf("The HTTP request failed with HTTP Code : %v - %v", resp.StatusCode, string(body))
		os.Exit(1)
	}

	item := &backend.Item{}
	json.Unmarshal(body, item)

	fmt.Printf("Item Created :\nValue: %v\n", item.Value)
}
