# training-go

GO Training Tour Exercise.

## Level

Beginner

## Topics

*  Map
*  Mutex in Read/Write 
*  Channel 
*  Dependency Injection
*  Server http 
*  Client http 
*  Unit test for Server http 
*  Unit test for Client http

## Content

The exercise is divided in three parts

### Backend 
 

Cache structure with lock on write and read
 

THe backend expose an interface with 5 methods:

1.     Add an element in the cache
2.     List all the elements 
3.     Find an element with its key
4.     Provide a channel to update an element 
5.     Provide a channel to delete an element

 

### Server HTTP 
 


Based on gorillia/mux and implements 4 routes

 
1.     Add (POST) : Add an element
2.     List all (GET) : List all elements 
3.     Find (GET) : Find an element 
4.     Update (PUT) : Update an element
5.     Delete (DELETE) : Delete an element


The backend is provided to the server using Dependency Injection.

### Client HTTP 
 
Application client:

A CLI app build with COBRA with the purpose to interact with the http server.

5 commands has to be implemented:

1.      add
2.      list
3.      find
4.      update
5.      delete


### Unit test 

1.     Backend
2.     Server HTTP 