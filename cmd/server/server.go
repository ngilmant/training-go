package main

import (
	"context"
	"github.com/gorilla/mux"
	"gitlab.com/ngilmant/training-go/pkg/backend"
	"gitlab.com/ngilmant/training-go/pkg/server"
	"gitlab.com/ngilmant/training-go/pkg/signals"
	"net/http"
	"os"
	"syscall"
	"time"
)

func main() {

	//Mux Router
	mux := mux.NewRouter()
	//Setup Routes
	m := backend.NewManager()
	h := server.NewHandler(m)
	h.SetupRoute(mux)
	//get Context
	//ctx, cancel := context.WithCancel(context.Background())
	ctx := signals.WithCancelSignals(
		context.Background(),
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGHUP,
		os.Interrupt,
	)

	//Start Backend Handler Listeners
	h.CM.Start(ctx)

	//Describe Server
	srv := &http.Server{
		Addr:         ":8080",
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  120 * time.Second,
		Handler:      mux,
	}

	// Start Server

	go func() {
		err := srv.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			panic(err)
		}
	}()

	<-ctx.Done()

	/*go func() {
		if err := srv.ListenAndServe(); err != nil {
			fmt.Println(err)
		}
	}()

	fmt.Printf("Server Started on port : %s \n", srv.Addr)
	//Graceful Shutdown Server
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	<- c

	fmt.Println("shutting down ...")
	cancel()
	srv.Shutdown(ctx)

	fmt.Println("shutting down terminated")
	os.Exit(0)*/
}
