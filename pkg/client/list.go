package client

import (
	"encoding/json"
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/ngilmant/training-go/pkg/backend"
	"io/ioutil"
	"net/http"
	"os"
)

// listCmd represents the list command
var listCmd = &cobra.Command{
	Use:   "list",
	Short: "List the items in the cache map",
	Long:  `List the items in the cache map`,
	Run:   list,
}

func init() {
	rootCmd.AddCommand(listCmd)
}

func list(cmd *cobra.Command, args []string) {

	resp, err := http.Get(httpConfig.BaseUrl + httpConfig.ListUrl)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %v\n", err)
		os.Exit(1)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		fmt.Printf("The HTTP body read failed with error %v\n", err)
		os.Exit(1)
	}

	if resp.StatusCode != http.StatusOK {
		fmt.Printf("The HTTP request failed with HTTP Code : %v - %v", resp.StatusCode, string(body))
		os.Exit(1)
	}

	items := &map[string]backend.Item{}
	json.Unmarshal(body, items)

	fmt.Printf("List of items:\nValue: %v\n", string(body))
}
