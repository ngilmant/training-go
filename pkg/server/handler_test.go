package server

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/require"
	"gitlab.com/ngilmant/training-go/pkg/backend"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

const (
	key_1        = "key_1"
	key_2        = "key_2"
	value_1      = "value_1"
	value_2      = "value_2"
	value_update = "updated Value"
)

func TestAddItem(t *testing.T) {

	m := backend.NewManager()
	h := NewHandler(m)
	ctx := context.Background()
	h.CM.Start(ctx)

	item := &backend.ItemRequest{
		key_1, backend.Item{
			value_1,
		},
	}

	body, err := json.Marshal(item)
	require.Nil(t, err)
	require.NotNil(t, body)

	req, err := http.NewRequest(http.MethodPost, "/addItem", bytes.NewReader(body))
	require.Nil(t, err)
	require.NotNil(t, req)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(h.addItem)

	handler.ServeHTTP(rr, req)

	require.Equal(t, http.StatusOK, rr.Code)
	require.Len(t, h.CM.List(), 1)
	require.Equal(t, h.CM.List()[key_1].Value, value_1)
}

func TestListItems(t *testing.T) {
	m := backend.NewManager()
	h := NewHandler(m)
	ctx := context.Background()
	h.CM.Start(ctx)

	req, err := http.NewRequest(http.MethodGet, "/listItems", nil)
	require.Nil(t, err)
	require.NotNil(t, req)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(h.listItem)

	handler.ServeHTTP(rr, req)

	require.Equal(t, http.StatusOK, rr.Code)

	items := &map[string]backend.Item{}

	body, err := ioutil.ReadAll(rr.Body)
	json.Unmarshal(body, items)
	require.Len(t, *items, 0)

	m.Add(key_1, value_1)
	handler.ServeHTTP(rr, req)

	require.Equal(t, http.StatusOK, rr.Code)

	body, err = ioutil.ReadAll(rr.Body)
	json.Unmarshal(body, items)
	require.Len(t, *items, 1)
}

func TestFindItem(t *testing.T) {
	m := backend.NewManager()
	h := NewHandler(m)
	ctx := context.Background()
	h.CM.Start(ctx)

	mux := mux.NewRouter()
	h.SetupRoute(mux)

	m.Add(key_1, value_1)
	m.Add(key_2, value_2)

	req, err := http.NewRequest(http.MethodGet, "/findItem/"+key_1, nil)

	require.Nil(t, err)
	require.NotNil(t, req)

	rr := httptest.NewRecorder()

	mux.ServeHTTP(rr, req)

	require.Equal(t, http.StatusOK, rr.Code)

	item := &backend.Item{}
	body, err := ioutil.ReadAll(rr.Body)
	json.Unmarshal(body, item)

	require.Equal(t, item.Value, value_1)
}

func TestUpdateItem(t *testing.T) {

	m := backend.NewManager()
	ctx := context.Background()
	m.Start(ctx)
	h := NewHandler(m)

	mux := mux.NewRouter()
	h.SetupRoute(mux)

	m.Add(key_1, value_1)
	m.Add(key_2, value_2)

	item := &backend.ItemRequest{
		key_1, backend.Item{
			value_update,
		},
	}

	body, err := json.Marshal(item)
	require.Nil(t, err)
	require.NotNil(t, body)

	req, err := http.NewRequest(http.MethodPut, "/updateItem", bytes.NewReader(body))
	require.Nil(t, err)
	require.NotNil(t, req)

	rr := httptest.NewRecorder()
	mux.ServeHTTP(rr, req)

	time.Sleep(2 * time.Second)
	require.Equal(t, http.StatusOK, rr.Code)
	require.Equal(t, m.List()[key_1].Value, value_update)
}

func TestDeleteItem(t *testing.T) {
	m := backend.NewManager()
	ctx := context.Background()
	m.Start(ctx)
	h := NewHandler(m)

	mux := mux.NewRouter()
	h.SetupRoute(mux)

	m.Add(key_1, value_1)
	m.Add(key_2, value_2)

	req, err := http.NewRequest(http.MethodDelete, "/deleteItem/"+key_2, nil)
	require.Nil(t, err)
	require.NotNil(t, req)

	rr := httptest.NewRecorder()
	mux.ServeHTTP(rr, req)

	time.Sleep(2 * time.Second)
	require.Equal(t, http.StatusOK, rr.Code)
	require.Len(t, h.CM.List(), 1)
}
